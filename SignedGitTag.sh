#!/bin/bash

# Mike Hannibal 03 February 2013 v1.0

# A menu driven interface to create a signed tag for Git
echo \n

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Press 'q' to exit Git Log screen and to continue"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"

git log

	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "What is the name for this tag? q to quit: "
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"
	read tagname

if [ "$tagname" = "q" ]; then
	exit
fi

	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "What is commit hash to apply the tag to? q to quit: "
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"
	read commithash

if [ "$commithash" = "q" ]; then
	exit
fi

	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "What is the message to add to this tag? q to quit: "
	echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"
	read tagmessage

if [ "$tagmessage" = "q" ]; then
	exit
fi
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"

tagmessage="'$tagmessage'"
echo "$tagname"
echo "$commithash"
echo "$tagmessage"


echo ""git tag -s" "$tagname"" -m ""$tagmessage" "$commithash"" > test.txt

tagcom=$(cat test.txt)

eval "$tagcom"

rm test.txt

# Usage from Git Help
# git tag [-a|-s|-u <key-id>] [-f] [-m <msg>|-F <file>] <tagname> [<head>]
