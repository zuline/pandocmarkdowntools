#!/bin/bash

# Mike Hannibal 02 February 2013 v1.0

# This script provides a series of menu choices and then executes an R script
#+to create a Markdown table suitable to convert using Pandoc.
# The menus are:
# 1) Select the directory you want to work in. It finds all directories below
#+$PWD
# 2) Enter a filename for the file to contain the finished table.
# 3) Select the file that contains tab-delimited data to be used in the table

# Create a path back to where we are

this=$PWD

# Shift to Dropbox

cd Dropbox

#find all the directories in the nominated directory and put them in "dirs"

dirs=$( ls -p $PWD | grep '/' )


# Create a prompt for the menu

PS3="Select the number of the directory you want to work in or 'q' to quit: "

# Create and run the menu


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"

select dirname in $dirs; do
	    if [ -n "$dirname" ]; then
	    	CDPATH=$PWD/"$dirname"
	    fi
	   break
done

if [ "$dirname" = "" ]; then
	exit
fi

cd $CDPATH


# Get the name for the outfile
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Enter the extended name of the output file. q to quit: "
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"
read outfile

if [ "$outfile" = "q" ]; then
	exit
fi
if [ "$outfile" = "" ]; then
	exit
fi

fname="$CDPATH""$outfile"


# Set the prompt for the select command

PS3="Type the number of the file to convert or 'q' to quit: "


# Next command restricts find to current directory.
fileList=$(find . -maxdepth 1 -name "*.*" -exec basename {} \;)



# Show a menu and ask for input. If the user entered a valid choice,
# then use the variables as the 2 args required by the R script and 
#+pass them and execute the R script which must be in the same original
#+directory as this script.

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"
select filename in $fileList; do
	    if [ -n "$filename" ]; then
	        infile="$CDPATH""$filename"
	        "$this""/""RTable.r" "$fname" "$infile"
	    fi
	    break
done

if [ "$filename" = "" ]; then
	exit
fi
