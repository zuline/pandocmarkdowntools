#!/usr/bin/Rscript

args <- commandArgs(trailingOnly = TRUE)
# print(args)
fname <- args[1]
infile <- args[2]

# Load the required R Library
library(pander)

# Nominate the output file
sink(fname)


x =read.table(file=infile,header=T)

pandoc.table(x)

sink()

