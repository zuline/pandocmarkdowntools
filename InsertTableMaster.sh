#!/bin/bash

# Create a path back to where we are

this=$PWD

# Shift to Dropbox

cd Dropbox

#find all the directories in the nominated directory and put them in "dirs"

dirs=$( ls -p $PWD | grep '/' )


# Create a prompt for the menu

PS3="Select the number of the directory you want to work in or 'q' to quit: "

# Create and run the menu


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"

select dirname in $dirs; do
	    if [ -n "$dirname" ]; then
	    	CDPATH=$PWD/"$dirname"
	    fi
	   break
done

if [ "$dirname" = "" ]; then
	exit
fi

cd $CDPATH


# Get the name for the output file
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "What do you want to name the result (incl extension)? 'q' to quit: "
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"
read outfile

if [ "$outfile" = "q" ]; then
	exit
fi
if [ "$outfile" = "" ]; then
	exit
fi

fname="$outfile"
echo $fname


# Set the prompt for the select command

PS3="Select the number of file with the markers in it. 'q' to quit: "


# Next command restricts find to current directory.
fileList=$(find . -maxdepth 1 -name "*.*" -exec basename {} \;)



# Show a menu and ask for input. If the user entered a valid choice,
#+then use the variable to run the script. 

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"
select markers in $fileList; do
	    if [ -n "$markers" ]; then
						# Read $markers file into a text file for later
						#processing. Create empty vars.
			cat "$markers" > try.txt
			LINE=""
			places=""
			
			# Now we scan the main file and create a variable and text file
			#containing a
			#+list of the markers.
			while read LINE; do
				places=$(awk '/@*@/ { print $0 }')
				echo "$places" > holder.txt
			done < "$markers"
			
			
			# Now we strip of the '@' from either end to give us the file name.
			#We send that
			#+to both a text file and a variable. Mainly for formatting reasons.
			strip=$(cat holder.txt | sed 's/@//g')
			echo "$strip" > search.txt
			
			# This is the real work. We read in the list of markers 'search.txt'
			#+line by line.
			# As we cycle through each line we search for that marker with sed.
			# The first pattern is the marker - quoting is really important.
			# The 'r' flag says 'read in the following file' - again quotes important.
			# The file is written in where the found pattern is.
			# The 'd' flag says 'now delete the marker'.
			# After each line we send the output file to the input file to replace the 
			#+contents so that we have an accumulation of changes as the input file is 
			#+read the next time.
			
			while read line1; do
				sed '/'"@$line1@"'/ {
						r '"$line1"'
						d
					}' <try.txt >delete.txt
					cat delete.txt > try.txt
			done < search.txt
	    fi
	    break
done

if [ "$markers" = "" ]; then
	exit
fi

# Send the output to the required file.
cat try.txt > "$fname"

# Clean up after ourselves.
rm delete.txt
rm try.txt
rm search.txt
rm holder.txt

