#!/bin/bash

# Do the find to populate the variable

fileList=$(find . -maxdepth 1 -name "*.sh" -exec basename {} \;)

# Create a prompt for the menu

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"

PS3="Select which script you wish to run or 'q' to quit: "

# Create and run the menu

select script in $fileList; do
	    if [ -n "$script" ]; then
	    	thisone="./""$script"
            "$thisone" # Run the script
	    fi
	    break
done
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"
