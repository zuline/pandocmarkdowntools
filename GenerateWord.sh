#!/bin/bash

# Mike Hannibal 29-01-2013

# This script creates a menu and asks for a directory choice
# Then it cd to the chosen directory
# Then it lists .md files in that directory to a menu
# Then it takes the menu choice and runs pandoc to convert to a docx file
# Then it returns to the originating directory

# NB: This script expects that you want to be searching for sub-directories in
# $HOME/Documents/Dropbox/
# Change the hard coding to search somewhere else.

this=$PWD

#find all the directories in the nominated directory and put them in "dirs"

dirs=$( ls -p $PWD/Dropbox/ | grep '/' )


# Create a prompt for the menu

PS3="Select the number of the directory you want to work in or 'q' to quit: "

# Create and run the menu

select dirname in $dirs; do
	    if [ -n "$dirname" ]; then
	    	mvg=$this/Dropbox/"$dirname"
	    fi
	    break
done


cd $mvg

# Set the prompt for the select command

PS3="Type the number of the .md to convert or 'q' to quit: "


# Next command restricts find to current directory.
fileList=$(find . -maxdepth 1 -name "*.md" -exec basename {} \;)



# Show a menu and ask for input. If the user entered a valid choice,
# then strip the .md and build the pandoc command
# execute the pandoc command

filename=" "

while [ "$filename" != "" ]; do
	select filename in $fileList; do
			if [ -n "$filename" ]; then
				# Strip the .md off the end of the filename
				filename=${filename%%.*}
				realfilename="$PWD""/""$filename"
				order="/usr/local/bin/pandoc "$filename".md -o "$filename".docx"
				echo "++++++++++++++++++++++++++++++"
				echo "File to be created with the name: "$filename".docx"
				echo "With the following command:"
				echo "$order"
				echo "++++++++++++++++++++++++++++++"
				echo "PLEASE check immediately below for error messages"
				echo "++++++++++++++++++++++++++++++"
				$order
	
			fi
			break 2
	done
done

